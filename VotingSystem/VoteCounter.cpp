#include "VoteCounter.h"
#include <iostream>

VoteCounter::VoteCounter(std::vector<BallotPaper> *ballots, std::vector<Candidate> *candidates) {
	this->ballots = ballots;
	this->candidates = candidates;
	for (unsigned int i = 0; i < candidates->size(); i++) {
		voteCount[candidates->at(i).getName()] = 0;
	}
	countVotes();
}

void VoteCounter::countVotes() {
	countFirstPreferences();
	int eliminatedCount = 0;
	std::string winner = "";
	for (int round = 0; round < candidates->size(); round++) {
		std::cout << std::endl << "Vote Count " << round + 1 << ":" << std::endl << std::endl;
		progressReport();
		for (int i = 0; i < candidates->size(); i++) {
			if (eliminatedCount == candidates->size() - 2) {
				winner = getWinner();
				break;
			}
			else if (voteCount.at(candidates->at(i).getName()) == getLowestVotes()) {
				eliminated.push_back(candidates->at(i));
				voteCount[eliminated.back().getName()] = -1;
				eliminatedCount++;
				std::cout << std::endl << eliminated.back().getName() << " has been eliminated." << std::endl << std::endl;
				break;
			}

		}
		if (winner != "") {
			break;
		}
		distributeVotes();
		system("pause");
	}
}

std::string VoteCounter::getWinner() {
	std::string winner;
	int highest = voteCount[candidates->at(0).getName()];
	for (int i = 1; i < candidates->size(); i++) {
		if (voteCount[candidates->at(i).getName()] > highest) {
			highest = voteCount[candidates->at(i).getName()];
		}
	}

	for (int i = 0; i < candidates->size(); i++) {
		if (voteCount[candidates->at(i).getName()] == highest) {
			winner = candidates->at(i).getName();
			std::cout << std::endl << "The winner is: " << candidates->at(i).getName() << std::endl << std::endl;
			break;
		}
	}
	return winner;
}

void VoteCounter::countFirstPreferences() {
	for (unsigned int i = 0; i < ballots->size(); i++) {
		voteCount[ballots->at(i).getPreference(1).getName()]++;

	}
}

void VoteCounter::distributeVotes() {
	for (unsigned int i = 0; i < ballots->size(); i++) {


		int j = 1;
		for (; j < candidates->size(); j++) {
			if (voteCount[ballots->at(i).getPreference(j).getName()] == -1) {
				continue;
			}
			else {
				break;
			}
			
		}

		for (int pref = 1; pref <= candidates->size(); pref++) {
			if (ballots->at(i).getPreference(pref).getName() == eliminated.back().getName() && j > pref) {
				voteCount[ballots->at(i).getPreference(j).getName()]++;
				break;

			}

		}

	}	

}

int VoteCounter::getLowestVotes() {
	int min = INT_MAX;
	for (int i = 0; i < candidates->size(); i++) {
		if (voteCount.at(candidates->at(i).getName()) < min && voteCount.at(candidates->at(i).getName()) > -1) {
			min = voteCount.at(candidates->at(i).getName());
		}

	}
	return min;

}
void VoteCounter::progressReport() {
	for (int i = 0; i < candidates->size(); i++) {
		if (voteCount.at(candidates->at(i).getName()) > -1) {
			std::cout << candidates->at(i).getName() << " : " << voteCount[candidates->at(i).getName()] << std::endl;
		}

	}
}

