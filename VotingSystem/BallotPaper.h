#pragma once
#include "Candidate.h"
#include <map>
#include <vector>

class BallotPaper {
private:

	std::map<int, Candidate> preferences;

public:
	void setPreference(int nthPreference, Candidate *cand);
	Candidate getPreference(int n);

};

