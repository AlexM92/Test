#pragma once
#include <string>

class Candidate {

	std::string name;
	std::string party;

public:
	Candidate();
	Candidate(std::string name, std::string party);
	std::string getName();
	std::string getParty();
	void setName(std::string name);
	void setParty(std::string party);
};
