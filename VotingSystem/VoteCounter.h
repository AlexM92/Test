#pragma once
#include "BallotPaper.h"

class VoteCounter {
public:
	VoteCounter(std::vector<BallotPaper> *ballots, std::vector<Candidate> *candidates);
private:
	std::vector<BallotPaper> *ballots;
	std::map<std::string, int> voteCount;
	std::vector<Candidate> *candidates;
	std::vector<Candidate> eliminated;
	void countVotes();
	int getLowestVotes();
	void progressReport();
	void distributeVotes();
	void countFirstPreferences();
	std::string getWinner();

};