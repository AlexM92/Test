#include <iostream>
#include <fstream>
#include <vector>
#include "BallotPaper.h"
#include "VoteCounter.h"

using namespace std;

int NO_OF_CANDIDATES;

int main() {
	//Read in the candidates
	ifstream myfile("votes.txt");
	string line;
	std::vector<Candidate> *candidates = new std::vector<Candidate>();;
	while (getline(myfile, line) && line != "-") {
		int splitIndex = line.find_first_of(",");
		Candidate newCandidate(line.substr(0, splitIndex), line.substr(splitIndex + 2, line.size()));
		candidates->push_back(newCandidate);
		NO_OF_CANDIDATES++;
	}
	
	//Read in the votes
	std::vector<BallotPaper> *ballots = new std::vector<BallotPaper>();
	while(getline(myfile, line)) {
		BallotPaper newBallot;
		for (int i = 0; i < NO_OF_CANDIDATES; i++) {
			newBallot.setPreference((line.at(i) - '0'), &candidates->at(i));
		}
		ballots->push_back(newBallot);
	}

	myfile.close();
	VoteCounter counter(ballots, candidates);
	system("pause");
	
	return 0;
}