#include "Candidate.h"

Candidate::Candidate() {

}

Candidate::Candidate(std::string name, std::string party) {
	this->name = name;
	this->party = party;
}

std::string Candidate::getName() {
	return this->name;
}

std::string Candidate::getParty() {
	return this->party;
}

void Candidate::setName(std::string name) {
	this->name = name;
}

void Candidate::setParty(std::string party) {
	this->party = party;
}
