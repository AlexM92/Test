#include "BallotPaper.h"
#include <iostream>

void BallotPaper::setPreference(int nthPreference, Candidate *cand) {
	this->preferences[nthPreference] = *cand;
}

Candidate BallotPaper::getPreference(int n) {
	return this->preferences[n];
}
